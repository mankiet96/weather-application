import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";

function searchLocation({ onUpdate }) {
    const handleBlur = ev => onUpdate(ev.target.value);

    const handleKeyDown = event => {
        if (event.key === "Enter") {
            onUpdate(event.target.value);

            // event.target.value = '';
        }
    };

    return <TextField autoFocus label="Enter location" onBlur={handleBlur} onKeyDown={handleKeyDown} />;
}

searchLocation.propTypes = {
    onUpdate: PropTypes.func.isRequired,
};

export default searchLocation;
