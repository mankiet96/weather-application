import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import WeatherCard from './weatherCard';
import { Button } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import SearchLocation from './searchLocation';

const useStyles = makeStyles(() => ({
	root: {
		flexGrow: 1,
		display: 'flex',
		flexDirection: 'column',
		height: '100vh',
		overflow: 'hidden'
	},
	containerGrid: {
		flex: 1,
		overflowY: 'auto',
		padding: '2em'
	},
	addButton: {
		position: 'absolute',
		margin: '1em',
		right: 0,
		bottom: 0
	}
}));

const LOCAL_STORAGE_KEY = 'locations';
function saveToLocalStorage(locations) {
	localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(locations));
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function readFromLocalStorage() {
	const storedLocations = localStorage.getItem(LOCAL_STORAGE_KEY);
	return storedLocations ? JSON.parse(storedLocations) : [];
}

function App() {
	const classes = useStyles();
	const [ weatherLocations, setWeatherLocations ] = React.useState(readFromLocalStorage());
	const [ isNewLocation, setIsNewLocation ] = useState(false);
	const [ saveLocation, setSaveLocation ] = useState(false);

	const handleAddClick = () => {
		console.log(isNewLocation);
		setWeatherLocations([ ...weatherLocations, '' ]);
		setIsNewLocation(true);
	}

	const updateLocations = (locations) => {
		setWeatherLocations(locations);
		saveToLocalStorage(locations);
	};

	const removeAtIndex = (index) => () =>
		updateLocations(weatherLocations.filter((_, locationIndex) => locationIndex !== index));

	const updateAtIndex = (index) => (updatedLocation) =>{
		updateLocations(
			weatherLocations.map((location, locationIndex) => (locationIndex === index ? updatedLocation : location))
		);
		setIsNewLocation(false);
		setSaveLocation(true);
	}

	const onClose = () => {
		setSaveLocation(false);
	}

	const canAddOrRemove = React.useMemo(() => weatherLocations.every((location) => location !== ''), [
		weatherLocations
	]);

	return (
		<div className={classes.root}>
			<Button
				onClick={handleAddClick}
				aria-label="add weather location"
				color="primary"
        variant="contained"
				disabled={!canAddOrRemove}
			>
				<AddIcon /> ADD NEW CITY
			</Button>
			{isNewLocation && <SearchLocation onUpdate={updateAtIndex(weatherLocations.length-1)} /> }
			<Snackbar open={saveLocation} autoHideDuration={3000} onClose={onClose}>
				<Alert severity="success">
					Successfully Added a new Location.
				</Alert>
			</Snackbar>
			<Grid container spacing={3} className={classes.containerGrid}>
				{weatherLocations.map((location, index) => (
					<Grid key={location} xs={12} sm={6} md={4} lg={3} item>
						<WeatherCard
							location={location}
							canDelete={!location || canAddOrRemove}
							onDelete={removeAtIndex(index)}
							onUpdate={updateAtIndex(index)}
						/>
					</Grid>
				))}
			</Grid>
		</div>
	);
}

export default App;
